John The Investor
========================

Welcome to John The Investor panel, a tool for store prices from 
companies in any market of the world.

The application is developed with Symfony Framework with
a MySQL database.

For the frontend Bootstrap 3 has been used, and the app is ready
for use in tablet and smartphone devices.

Created entities
--------
* Market
* Company
* Stock
* StockType

Quickstart
----------

Go to project directory:

1. Edit app/config/parameters.yml and set your database credentials.
2. Execute composer install for download all needed vendor and packages.
3. Execute php bin/console server:run and go to http://127.0.0.1:8000
4. For execute some tests execute php vendor/bin/simple-phpunit


TODO:
-----

* Fix responsive in tables.
* Dockerize app
* Cache results in a Redis/Memcached system
* Improve look & feel.
* (...)

You can see running a 'live' version in here http://eqs.projectsoldiers.com/