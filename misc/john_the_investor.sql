-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 10-12-2017 a las 18:26:18
-- Versión del servidor: 5.7.20-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.25-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `eqs`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company_stock`
--

CREATE TABLE `company_stock` (
  `id` int(11) NOT NULL,
  `stock_type_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `market_id` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `market`
--

CREATE TABLE `market` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `market`
--

INSERT INTO `market` (`id`, `name`) VALUES
(1, 'New York Stock Exchange'),
(2, 'London Stock Exchange'),
(3, 'Hong Kong Stock Exchange'),
(4, 'Shangai Stock Exchange'),
(5, 'Deutsche Börse Frankfurt');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock_type`
--

CREATE TABLE `stock_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `stock_type`
--

INSERT INTO `stock_type` (`id`, `name`) VALUES
(1, 'Preferred Stock'),
(2, 'Common Stock');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `company_stock`
--
ALTER TABLE `company_stock`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `market_company_stock` (`stock_type_id`,`company_id`,`market_id`),
  ADD KEY `IDX_CC7F405DCEA9C30` (`stock_type_id`),
  ADD KEY `IDX_CC7F405979B1AD6` (`company_id`),
  ADD KEY `IDX_CC7F405622F3F37` (`market_id`);

--
-- Indices de la tabla `market`
--
ALTER TABLE `market`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `stock_type`
--
ALTER TABLE `stock_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `company_stock`
--
ALTER TABLE `company_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT de la tabla `market`
--
ALTER TABLE `market`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `stock_type`
--
ALTER TABLE `stock_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `company_stock`
--
ALTER TABLE `company_stock`
  ADD CONSTRAINT `FK_CC7F405622F3F37` FOREIGN KEY (`market_id`) REFERENCES `market` (`id`),
  ADD CONSTRAINT `FK_CC7F405979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_CC7F405DCEA9C30` FOREIGN KEY (`stock_type_id`) REFERENCES `stock_type` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;