<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="company_stock", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="market_company_stock", columns={"stock_type_id", "company_id","market_id"})
 * })
 */
class CompanyStock
{
    const CURRENCY = "€";

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="StockType")
     */
    private $stockType;

    /**
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="Company")
     */
    private $company;

    /**
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="Market")
     */
    private $market;

    /**
     * @Assert\NotNull()
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime")
     * @ORM\Version
     */
    private $created;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStockType()
    {
        return $this->stockType;
    }

    /**
     * @param mixed $stockType
     */
    public function setStockType($stockType)
    {
        $this->stockType = $stockType;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getMarket()
    {
        return $this->market;
    }

    /**
     * @param mixed $market
     */
    public function setMarket($market)
    {
        $this->market = $market;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @param mixed $price
     */
    public function __toString()
    {
        return "{$this->price}";
    }
}
