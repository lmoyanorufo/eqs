<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CompanyStock;
use AppBundle\Entity\Market;
use Doctrine\Common\Collections\Collection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Market controller.
 *
 * @Route("market")
 */
class MarketController extends Controller
{
    /**
     * Lists all market entities.
     *
     * @Route("/", name="market_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $markets = $em->getRepository('AppBundle:Market')->findAll();

        return $this->render('market/index.html.twig', array(
            'markets' => $markets,
        ));
    }

    /**
     * Lists all market entities.
     *
     * @Route("/highestValues", name="highest_values")
     * @Method("GET")
     */
    public function highestValuesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $markets = $em->getRepository('AppBundle:Market')->findAll();

        $maxValuesMarkets = [];

        /** @var Market $market */
        foreach ($markets as $market) {
            $maxValuesMarkets[] = [
                'market'   => $market,
                'maxPrice' =>  $this->getHighestCompanyStock($market->getCompanyStocks())
            ];
        }

        return $this->render('market/maxPriceMarkets.html.twig', array(
            'maxValuesMarkets' => $maxValuesMarkets,
            'currencySymbol'   => CompanyStock::CURRENCY
        ));
    }

    /**
     * Finds and displays a market entity.
     *
     * @Route("/{id}", name="market_show")
     * @Method("GET")
     */
    public function showAction(Market $market)
    {
        return $this->render('market/show.html.twig', [
            'market'         => $market,
            'companyStocks'  => $market->getCompanyStocks(),
            'currencySymbol' => CompanyStock::CURRENCY
        ]);
    }

    /**
     * @param Collection|CompanyStock[] $companyStocks
     * @return CompanyStock
     */
    private function getHighestCompanyStock($companyStocks)
    {
        $maxPrice = 0;
        $maxCompanyStock = null;
        foreach($companyStocks as $companyStock) {

            if ($maxPrice < $companyStock->getPrice()) {
                $maxCompanyStock = $companyStock;
                $maxPrice = $companyStock->getPrice();
            }
        }

        return $maxCompanyStock;
    }
}
