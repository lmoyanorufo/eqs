<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CompanyStock;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/**
 * Companystock controller.
 *
 * @Route("companystock")
 */
class CompanyStockController extends Controller
{
    /**
     * Creates a new companyStock entity.
     *
     * @Route("/new", name="companystock_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $companyStock = new Companystock();
        $form = $this->createForm('AppBundle\Form\CompanyStockType', $companyStock);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            try {
                $em->persist($companyStock);
                $em->flush();
            } catch (UniqueConstraintViolationException $ex) {
                $this->updateStockPrice($companyStock);
            }

            return $this->redirectToRoute('companystock_overview');
        }

        return $this->render('companystock/new.html.twig', array(
            'companyStock' => $companyStock,
            'form' => $form->createView(),
        ));
    }


    /**
     * Lists all companyStock displaying all values in markets.
     * In terms of performance could be improved with a query, making joins
     * Of all objects. (Review this comment)
     *
     * @Route("/overview", name="companystock_overview")
     * @Method("GET")
     */
    public function overviewAction()
    {
        $em = $this->getDoctrine()->getManager();

        $companyStocks = $em->getRepository('AppBundle:CompanyStock')->findAll();

        $stocksCompanyAndType = [];

        /** @var CompanyStock $companyStock */
        foreach ($companyStocks as $companyStock) {

            $company   = $companyStock->getCompany();
            $stockType = $companyStock->getStockType();
            $key = "{$company->getId()}-{$companyStock->getStockType()->getId()}";

            if (!array_key_exists($key, $stocksCompanyAndType)) {
                $stocksCompanyAndType[$key] = [
                    'company'      => $company,
                    'stockType'    => $stockType,
                    'listStocks'   => []
                ];
            }

            $stocksCompanyAndType[$key]['listStocks'][] = $companyStock;
        }

        return $this->render('companystock/overview.html.twig', [
            'stocksCompanyAndType' => $stocksCompanyAndType,
            'currencySymbol'       => CompanyStock::CURRENCY
        ]);
    }

    /**
     * @param   CompanyStock $existingStockValue
     * @return  CompanyStock
     */
    private function updateStockPrice($existingStockValue)
    {
        $container = $this->container;
        $container->set('doctrine.orm.entity_manager', null);
        $container->set('doctrine.orm.default_entity_manager', null);
        $em = $this->getDoctrine()->getManager();

        /** @var CompanyStock $existingEntity */
        $existingEntity = $em->getRepository('AppBundle:CompanyStock')
            ->findOneBy([
                'market'    => $existingStockValue->getMarket(),
                'stockType' => $existingStockValue->getStockType(),
                'company'   => $existingStockValue->getCompany()
            ]);

        $existingEntity->setPrice($existingStockValue->getPrice());
        $em->persist($existingEntity);
        $em->flush();

        return $existingEntity;
    }
}
