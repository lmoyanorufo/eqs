<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MarketController extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/market/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Markets list', $crawler->filter('h1')->text());
    }

    public function testHighestValues()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/market/highestValues');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Highest Market prices', $crawler->filter('h1')->text());
    }
}
