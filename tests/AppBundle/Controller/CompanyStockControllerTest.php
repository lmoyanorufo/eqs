<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CompanyStockControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/companystock/overview');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Company stock overview', $crawler->filter('h1')->text());
    }

    public function testNew()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/companystock/new');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('New company stock', $crawler->filter('h1')->text());
    }
}