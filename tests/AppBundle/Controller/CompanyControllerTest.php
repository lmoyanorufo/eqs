<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CompanyControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/company/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('List companies', $crawler->filter('h1')->text());
    }

    public function testNew()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/company/new');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('New company', $crawler->filter('h1')->text());
    }
}
